import 'package:flutter/material.dart';

import 'screen2.dart';

class Screen1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: const Text('Screen 1'),
      ),
      body: Center(
        child: TextButton(
          style: TextButton.styleFrom(
            elevation: 2.0,
            backgroundColor: Colors.red,
          ),
          child: const Text(
            'Go Forwards To Screen 2',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Screen2()),
            );
          },
        ),
      ),
    );
  }
}
