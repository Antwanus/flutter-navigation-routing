import 'package:flutter/material.dart';
import 'package:flutter_navigation_routing/screen0.dart';
import 'package:flutter_navigation_routing/screen1.dart';
import 'package:flutter_navigation_routing/screen2.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Screen0(),
      routes: {
        '/screen1': (context) => Screen1(),
        '/screen2': (context) => Screen2(),
      },
    );
  }
}
